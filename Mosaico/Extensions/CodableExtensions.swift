import Foundation

extension Decodable {
    static func decode(_ data: Data?) -> Self? {
        guard let data = data else { return nil }
        return try? JSONDecoder().decode(self, from: data)
    }
}
