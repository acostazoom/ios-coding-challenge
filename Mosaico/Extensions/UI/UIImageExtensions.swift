import UIKit

extension UIImage {
    static func rounded(backgroundColor: UIColor, size: CGFloat) -> UIImage? {
        let contextRect = CGRect(origin: .zero, size: CGSize(width: size, height: size))
        
        UIGraphicsBeginImageContextWithOptions(contextRect.size, false, UIScreen.main.scale)

        if let context = UIGraphicsGetCurrentContext() {
            context.addEllipse(in: contextRect)
            context.setFillColor(backgroundColor.cgColor)
            context.fillPath()
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()

            return image
        }

        return nil
    }
    
    func withPadding(padding: CGFloat) -> UIImage? {
        let contextRect = CGRect(origin: .zero, size: size)
        
        UIGraphicsBeginImageContextWithOptions(contextRect.size, false, UIScreen.main.scale)

        if let context = UIGraphicsGetCurrentContext() {
            context.addEllipse(in: contextRect)
            context.setFillColor(UIColor.clear.cgColor)
            context.fillPath()
            draw(in: CGRect(x: padding, y: padding, width: size.width - (padding * 2), height: size.height - (padding * 2)))
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()

            return image
        }

        return nil
    }
}
