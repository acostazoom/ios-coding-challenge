import UIKit
import Kingfisher


extension KingfisherWrapper where Base == UIImage {
    func offsetingSize(x: CGFloat, y: CGFloat) -> KFCrossPlatformImage {
        return resize(to: CGSize(width: base.size.width + x, height: base.size.height + y))
    }
}

extension KingfisherWrapper where Base == UIImageView {
    func setImage(_ url: URL?) {
        if let image = url {
            self.setImage(
                with: image,
                placeholder: nil,
                options: [.transition(.fade(0.35))]
            )
        } else {
            base.image = nil
        }
    }
}
