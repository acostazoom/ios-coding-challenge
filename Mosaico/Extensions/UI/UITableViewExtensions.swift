import UIKit

extension UITableView {    
    func registerCell<T: UITableViewCell>(_ cell: T.Type) {
        register(cell, forCellReuseIdentifier: String(describing: cell))
    }

    func dequeueCell<T: UITableViewCell>(_ identifier: String = String(describing: T.self), for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! T
    }
}
