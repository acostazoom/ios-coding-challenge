import Foundation

extension Collection {
    func get(at i: Index) -> Element? {
        return indices.contains(i) ? self[i] : nil
    }
}

extension Collection where Index == Int {
    var lastIndex: Int { return (count - 1) }
}

extension Array where Element: Equatable {
    mutating func appendIfNotContains(_ element: Element) {
        if !contains(element) {
            append(element)
        }
    }
    
    func appendingIfNotContains(contentsOf sequence: Self) -> Self {
        var new = self
        for element in sequence {
            new.appendIfNotContains(element)
        }
        return new
    }
}

extension Optional where Wrapped: Collection {
    var isNullOrEmpty: Bool { return self == nil || self?.isEmpty == true }
}

func +<Key, Value> (lhs: [Key: Value], rhs: [Key: Value]) -> [Key: Value] {
    var dict = lhs
    rhs.forEach { dict[$0] = $1 }
    return dict
}
