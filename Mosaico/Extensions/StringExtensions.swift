
import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }

    func localized(_ args: CVarArg...) -> String {
        let localizedStr = localized()
        return withVaList(args) {
            return NSString(format: localizedStr, arguments: $0) as String
        }
    }

}
