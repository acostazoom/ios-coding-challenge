
import Foundation


class DataPaginationController<TSuccessElement, TError> {
    private(set) var state = ObservableState<TSuccess, TError>()
    
    private(set) var currentPage: Int?
    private(set) var maxPages: Int = 1
    
    var getPageFunction: RequestPage?
    
    var proccessPageResultFunction: ProccessPaginationResult
    
    init() where TSuccessElement: Equatable {
        proccessPageResultFunction = { currentData, pageData in
            let previous = (currentData ?? [])
            let new = previous.appendingIfNotContains(contentsOf: pageData.result)
            return .init(data: new, hasNewData: new.count > previous.count)
        }
    }
    
    init() {
        proccessPageResultFunction = { currentData, pageData in
            let data = (currentData ?? []) + pageData.result
            return .init(data: data, hasNewData: true)
        }
    }
    
    func getFirstPage() {
        getPage(0, data: nil)
    }
    
    func getNextPage() {
        guard !state.isLoading else { return }
        getPage(1 + (self.currentPage ?? -1), data: state.data)
    }
    
    private func getPage(_ page: Int, data: TSuccess?) {
        guard page < maxPages,
              let pageLoader = getPageFunction else { return }
        
        state.value = .loading(data: data)
        
        pageLoader(page) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let pageResult):
                let result = self.proccessPageResultFunction(self.state.data, pageResult)
                let pages = result.hasNewData ? pageResult.maxPages : pageResult.currentPage // Interrompe a paginação quando não há mais dados novos vindos do request.
                self.maxPages = max(pages, 1) // Precisa ser sempre no minimo 1, pra que seja sempre possível ao menos consultar a primeira pagina
                self.currentPage = pageResult.currentPage
                self.state.value = .loaded(data: result.data)
            case .error(let error):
                if let currentData = self.state.data {
                    self.state.value = .partialFailure(error: error, data: currentData)
                } else {
                    self.state.value = .failure(error: error)
                }
            case .cancelled:
                self.state.value = .loaded(data: self.state.data ?? [])                
            }
        }
    }

    // MARK: - Inner Types
    typealias TSuccess = Array<TSuccessElement>
    typealias RequestPageFunctionCallback = (RequestResult) -> Void // Função que retorna para o DataPaginationController o resultado dos dados da pagina requisitada
    typealias RequestPage = (Int, @escaping RequestPageFunctionCallback) -> Void // Função que é responsável por retornar os dados da pagina atual através do callback
    typealias ProccessPaginationResult = (TSuccess?, RequestPageResult<TSuccess>) -> ProccessedPageData // Função que é responsável por processar e combinar os dados atuais com os dados da nova página

    enum RequestResult {
        case success(_ data: RequestPageResult<TSuccess>)
        case error(_ error: TError)
        case cancelled
    }
    
    struct ProccessedPageData {
        let data: TSuccess
        let hasNewData: Bool
    }
}
