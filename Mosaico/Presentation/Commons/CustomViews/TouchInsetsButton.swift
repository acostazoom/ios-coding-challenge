import UIKit

class TouchInsetsButton: UIButton {
    var touchInsets = UIEdgeInsets(top: -5, left: -5, bottom: -5, right: -5)
    
    convenience init() {
        self.init(type: .system)
    }

    override func point(inside point: CGPoint, with _: UIEvent?) -> Bool {
        let area = bounds.inset(by: touchInsets)
        let contains = area.contains(point)
        return contains
    }    
}
