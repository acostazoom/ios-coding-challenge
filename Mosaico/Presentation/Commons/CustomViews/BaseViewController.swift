import UIKit
import SnapKit
import JGProgressHUD

class BaseViewController: UIViewController {
    private let viewModel: BaseViewModelProtocol?
    
    lazy var keyboardHandlerAdapter: KeyboardHandlerAdapterProtocol = KeyboardHandlerAdapter(view)
    
    lazy var loadingView = JGProgressHUD()
        
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(viewModel: BaseViewModelProtocol?) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) not implemented")
    }
    
    override func loadView() {
        self.view = UIView()
        createViews()
        layoutViews()
        observeViewModel()
    }
            
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.viewWillDisappear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.viewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel?.viewDidDisappear()
    }
    
    func createViews() {
        view.backgroundColor = .white
        
        navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.barStyle = .black
        // create views nas subclasses
    }
    
    func layoutViews() {
        // layout views nas subclasses
    }
        
    func observeViewModel() {
        // observar viewmodel nas subclasses
    }
}
