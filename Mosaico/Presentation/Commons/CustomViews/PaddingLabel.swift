
import UIKit

class PaddingLabel: UILabel {
    var padding = UIEdgeInsets.zero

    override func drawText(in rect: CGRect) {
        if text.isNullOrEmpty {
            super.drawText(in: rect)
        } else {
            super.drawText(in: rect.inset(by: padding))
        }
    }

    override var intrinsicContentSize: CGSize {
        if text.isNullOrEmpty {
            return CGSize.zero
        } else {
            return addingInsets(to: super.intrinsicContentSize)
        }
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        if text.isNullOrEmpty {
            return CGSize.zero
        } else {
            return addingInsets(to: size)
        }
    }

    private func addingInsets(to size: CGSize) -> CGSize {
        return CGSize(
            width: size.width + padding.left + padding.right,
            height: size.height + padding.top + padding.bottom
        )
    }
}
