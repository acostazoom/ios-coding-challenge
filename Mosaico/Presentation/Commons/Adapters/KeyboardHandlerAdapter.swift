
import UIKit

protocol KeyboardHandlerAdapterProtocol: AnyObject {
    var originalConstant: CGFloat { get set }
    var displayingKeyboardMargin: CGFloat { get set }
    var bottomConstraint: NSLayoutConstraint? { get set }
}

class KeyboardHandlerAdapter: NSObject,
                              KeyboardHandlerAdapterProtocol {
    private weak var view: UIView?
    
    var originalConstant: CGFloat = 0
    var displayingKeyboardMargin: CGFloat = 12
    
    weak var bottomConstraint: NSLayoutConstraint? {
        didSet {
            originalConstant = bottomConstraint?.constant ?? 0
        }
    }

    init(_ view: UIView) {
        super.init()
        self.view = view
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardChangeFrame), name: UIResponder.keyboardDidChangeFrameNotification, object: nil)
    }    
    
    @objc private func onKeyboardShow(notification: Notification) {
        guard let view = self.view,
              let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        bottomConstraint?.constant = -(keyboardSize.height + displayingKeyboardMargin - view.safeAreaInsets.bottom)
        
        UIView.transition(with: view, duration: 0.2, options: .curveEaseOut) {
            view.layoutIfNeeded()
        } completion: { _ in }
    }
    
    @objc private func onKeyboardHide(notification: Notification) {
        guard let view = self.view else { return }
        
        bottomConstraint?.constant = originalConstant
        
        UIView.transition(with: view, duration: 0.2, options: .curveEaseIn) {
            view.layoutIfNeeded()
        } completion: { _ in }
    }
    
    @objc private func onKeyboardChangeFrame(notification: Notification) {
        guard let view = self.view,
              let currentOffset = bottomConstraint?.constant, currentOffset > 0,
              let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        bottomConstraint?.constant = -(keyboardSize.height + displayingKeyboardMargin - view.safeAreaInsets.bottom)
    }
}
