import Foundation

enum ViewModelDataState<TSuccess, TError> {
    case loading(data: TSuccess?) // Quando a informação está atualizando, e pode possuir ou não um estado pré-carregado enquanto atualiza.
    case loaded(data: TSuccess) // Quando a informação está atualizada, e possui um estado válido.
    case failure(error: TError) // Quando a informação falhou completamente em atualizar, e possui um estado de erro.
    case partialFailure(error: TError, data: TSuccess) // Quando a informação falhou parcialmente em atualizar, contendo tanto o estado de erro como um estado válido.
    case notLoaded // Quando a informação está no estado inicial.

    var data: TSuccess? {
        switch self {
        case let .loaded(data): return data
        case let .loading(data): return data
        case let .partialFailure(_, data): return data
        default: return nil
        }
    }

    var error: TError? {
        switch self {
        case .failure(let error),
             .partialFailure(let error, _): return error
        default: return nil
        }
    }

    var isLoading: Bool {
        if case .loading = self {
            return true
        } else {
            return false
        }
    }

    var isNotLoaded: Bool {
        if case .notLoaded = self {
            return true
        } else {
            return false
        }
    }
        
}

extension ViewModelDataState: Equatable where TSuccess: Equatable, TError: Equatable {
    static func == (lhs: ViewModelDataState<TSuccess, TError>, rhs: ViewModelDataState<TSuccess, TError>) -> Bool {
        switch (lhs, rhs) {
        case (.notLoaded, .notLoaded): return true
        case let (.loading(lhsData), .loading(rhsData)): return lhsData == rhsData
        case let (.loaded(lhsData), .loaded(rhsData)): return lhsData == rhsData
        case let (.failure(lhsError), .failure(rhsError)): return lhsError == rhsError
        case let (.partialFailure(lhsError, lhsData), .partialFailure(rhsError, rhsData)): return lhsError == rhsError && lhsData == rhsData
        default: return false
        }
    }
}

class ObservableState<TSuccess, TError>: ObservableData<ViewModelDataState<TSuccess, TError>> {
    var data: TSuccess? { return value.data }

    var error: TError? { return value.error }

    var isLoading: Bool { return value.isLoading }

    var isNotLoaded: Bool { return value.isNotLoaded }

    convenience init() {
        self.init(.notLoaded)
    }
}

class ObservableData<TState> {
    private var observerHandler: ((_ data: TState) -> Void)?

    var value: TState {
        didSet {
            notifyObserver()
        }
    }

    init(_ initialState: TState) {
        value = initialState
    }

    func observe(_ handler: @escaping (_ data: TState) -> Void) {
        observerHandler = handler
    }

    func notifyObserver() {
        notifyObserver(value: value)
    }
    
    private func notifyObserver(value: TState) {
        guard let handler = observerHandler else { return }

        if Thread.isMainThread {
            handler(value)
        } else {
            DispatchQueue.main.async {
                handler(value)
            }
        }
    }
}
