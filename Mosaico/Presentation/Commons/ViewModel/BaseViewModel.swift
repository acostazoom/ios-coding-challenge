import Foundation

protocol BaseViewModelProtocol {
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()
}

enum ViewAppearDataReloadRule {
    case never
    case always
    case after(time: TimeInterval)
}

class BaseViewModel: BaseViewModelProtocol {
    private var lastUpdate: Date?
    
    var reloadRule: ViewAppearDataReloadRule { return .never }

    func viewDidLoad() {
        //
    }
    
    func viewWillAppear() {
        validateReload()
    }
    
    func viewDidAppear() {
        //
    }
    
    func viewWillDisappear() {
        //
    }
    
    func viewDidDisappear() {
        //
    }
    
    func invalidateReload() {
        lastUpdate = nil
    }
    
    func validateReload() {
        switch reloadRule {
        case .never:
            if lastUpdate == nil {
                lastUpdate = Date()
                loadNewData()
            }
        case .always:
            lastUpdate = Date()
            loadNewData()
        case .after(let time):
            let now = Date()
            let updateDate = (lastUpdate ?? Date.distantPast).addingTimeInterval(time)
            
            if updateDate <= now {
                lastUpdate = now
                loadNewData()
            }
        }
    }
    
    func loadNewData() {
        //
    }
}
