
import UIKit
import WebKit

class WebPageViewController: BaseViewController,
                                WKNavigationDelegate {
    
    // View properties
    let webView: WKWebView = {
        let configuration = WKWebViewConfiguration()
        return WKWebView(frame: .zero, configuration: configuration)
    }()
    
    // Behaviour properties
    private let viewModel: WebPageViewModelProtocol
    
    init(url: URL) {
        self.viewModel = WebPageViewModel(url: url)
        super.init(viewModel: viewModel)
    }
    
    override func createViews() {
        super.createViews()
                        
        // web view
        webView.navigationDelegate = self
        webView.scrollView.contentInsetAdjustmentBehavior = .never
        view.addSubview(webView)
        
        // search button
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "search")?.withPadding(padding: 5),
            style: .plain,
            target: self,
            action: #selector(onSearchButton)
        )
    }
    
    override func layoutViews() {
        super.layoutViews()
        
        webView.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.webPageUrl.observe { [weak self] _ in
            self?.onViewModelWebPageUrlChanged()
        }
    }
    
    // MARK: - ViewModel
    private func onViewModelWebPageUrlChanged() {
        displayLoading()
        webView.load(URLRequest(url: viewModel.webPageUrl.value))
    }
    
    func displayLoading() {
        webView.alpha = 0
        loadingView.show(in: view, animated: true)
    }
    
    func hideLoading() {
        loadingView.dismiss(animated: true)
        UIView.animate(withDuration: 0.2) {
            self.webView.alpha = 1
        }
    }
    
    // MARK: - WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoading()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideLoading()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        hideLoading()
    }
    
    // MARK: - User action
    @objc private func onSearchButton() {
        navigationController?.pushViewController(SearchViewController(), animated: true)
    }
}
