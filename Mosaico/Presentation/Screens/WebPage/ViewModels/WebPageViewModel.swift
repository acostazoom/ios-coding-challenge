
import Foundation

protocol WebPageViewModelProtocol: BaseViewModelProtocol {
    var webPageUrl: ObservableData<URL> { get }
}


class WebPageViewModel: BaseViewModel, WebPageViewModelProtocol {
    let webPageUrl: ObservableData<URL>
    
    init(url: URL) {
        self.webPageUrl = .init(url)
    }
    
    override func loadNewData() {
        webPageUrl.notifyObserver()
    }
}

