
import UIKit

protocol RelatedArticlesAdapterDelegate: AnyObject {
    func onSelectedRelatedArticle(_ adapter: RelatedArticlesAdapterProtocol, _ relatedArticle: SearchViewModel.RelatedArticle, at index: Int)
    func onDisplayLastRelatedArticle(_ adapter: RelatedArticlesAdapterProtocol)
}

protocol RelatedArticlesAdapterProtocol: AnyObject {
    var delegate: RelatedArticlesAdapterDelegate? { get set }
    var itemSize: CGSize { get }
    
    func reload(with data: [SearchViewModel.RelatedArticle])
}

class RelatedArticlesCollectionAdapter: NSObject,
                                          RelatedArticlesAdapterProtocol,
                                          UICollectionViewDataSource,
                                          UICollectionViewDelegateFlowLayout {
    
    let itemSize: CGSize
    weak var delegate: RelatedArticlesAdapterDelegate?
        
    private weak var collectionView: UICollectionView!
    
    private var data: [SearchViewModel.RelatedArticle] = []
    
    init(_ collectionView: UICollectionView, itemSize: CGSize = .init(width: UIScreen.main.bounds.width * 0.8, height: 100)) {
        self.collectionView = collectionView
        self.itemSize = itemSize
        super.init()
        setupCollectionView()
    }
    
    func reload(with data: [SearchViewModel.RelatedArticle]) {
        self.data = data
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RelatedArticleCollectionViewCell = collectionView.dequeueCell(for: indexPath)
        if let article = data.get(at: indexPath.row) {
            cell.bind(article)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let article = data.get(at: indexPath.row) else { return }
        delegate?.onSelectedRelatedArticle(self, article, at: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard indexPath.row == data.lastIndex else { return }
        delegate?.onDisplayLastRelatedArticle(self)
    }

    private func setupCollectionView() {
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.clipsToBounds = false
        collectionView.contentInset = .init(top: 0, left: 16, bottom: 0, right: 16)
        collectionView.contentOffset.x = -collectionView.contentInset.left
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.flowLayout?.itemSize = itemSize
        collectionView.flowLayout?.scrollDirection = .horizontal
        collectionView.registerCell(RelatedArticleCollectionViewCell.self)
    }
}
