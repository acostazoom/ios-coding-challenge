
import UIKit

protocol QueryHintAdapterDelegate: AnyObject {
    func onSelectQueryHint(_ adapter: QueryHintAdapterProtocol, hint: String, at index: Int)
    func onDisplayLastQueryHint(_ adapter: QueryHintAdapterProtocol)
}

protocol QueryHintAdapterProtocol {
    var delegate: QueryHintAdapterDelegate? { get set }
    
    func reload(with data: [String])
}

class QueryHintTableAdapter: NSObject,
                               QueryHintAdapterProtocol,
                               UITableViewDelegate,
                               UITableViewDataSource {

    weak var delegate: QueryHintAdapterDelegate?
    
    private weak var tableView: UITableView!
    
    private var data: [String] = []
    
    init(_ tableView: UITableView) {
        self.tableView = tableView
        super.init()
        setupTable()
    }
    
    func reload(with data: [String]) {
        self.data = data
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = PaddingLabel()
        label.text = "SuggestionsLabel".localized()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.backgroundColor = UIColor.init(white: 0.8, alpha: 1)
        label.padding = .init(top: -5, left: 20, bottom: -5, right: 0)
        return label
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueCell(for: indexPath)
        cell.textLabel?.text = data.get(at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let hint = data.get(at: indexPath.row) else { return }
        delegate?.onSelectQueryHint(self, hint: hint, at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == data.lastIndex else { return }
        delegate?.onDisplayLastQueryHint(self)
    }
    
    private func setupTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = 44
        tableView.registerCell(UITableViewCell.self)
    }
}
