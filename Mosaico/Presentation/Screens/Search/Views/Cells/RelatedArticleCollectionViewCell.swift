
import UIKit

class RelatedArticleCollectionViewCell: BaseCollectionViewCell {
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    
    func bind(_ relatedArticle: SearchViewModel.RelatedArticle) {
        imageView.kf.setImage(relatedArticle.imageUrl)
        titleLabel.text = relatedArticle.title
    }
    
    override func createViews() {
        super.createViews()
        
        // image view
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .lightGray
        contentView.addSubview(imageView)
        
        // title label
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        titleLabel.numberOfLines = 0
        contentView.addSubview(titleLabel)
        
        // shadow and border
        contentView.clipsToBounds = true
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 10
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.lightGray.cgColor
        
        layer.shadowOffset = .init(width: 3, height: 3)
        layer.shadowRadius = 4
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.7
    }
    
    override func layoutViews() {
        super.layoutViews()
        
        imageView.snp.makeConstraints {
            $0.leading.top.bottom.equalToSuperview()
            $0.width.equalTo(imageView.snp.height).offset(25)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview().inset(16)
            $0.leading.equalTo(imageView.snp.trailing).offset(16)
            $0.bottom.lessThanOrEqualToSuperview().inset(16)
        }
    }
}
