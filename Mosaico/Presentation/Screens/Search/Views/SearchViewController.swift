
import UIKit

class SearchViewController: BaseViewController,
                            UISearchBarDelegate,
                            QueryHintAdapterDelegate,
                            RelatedArticlesAdapterDelegate {
    // View properties
    let searchBar = UISearchBar()
    let voiceSearchButton = TouchInsetsButton()
    let closeButton = TouchInsetsButton()
    let tableView = UITableView()
    let emptyStateLabel = UILabel()
    let relatedArticlesStackView = UIStackView()
    let relatedArticlesTitleLabel = PaddingLabel()
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    // Behavior properties
    private let viewModel: SearchViewModelProtocol
    private lazy var queryHintsAdapter: QueryHintAdapterProtocol = QueryHintTableAdapter(tableView)
    private lazy var relatedArticlesAdapter: RelatedArticlesAdapterProtocol = RelatedArticlesCollectionAdapter(collectionView)
    
    convenience init() {
        self.init(SearchViewModel.Factory().getInstance())
    }
    
    init(_ viewModel: SearchViewModelProtocol) {
        self.viewModel = viewModel
        super.init(viewModel: viewModel)
    }
    
    override func createViews() {
        super.createViews()
                
        // search bar
        searchBar.searchBarStyle = .minimal
        searchBar.autocapitalizationType = .none
        searchBar.delegate = self
        searchBar.setSearchFieldBackgroundImage(UIImage.rounded(backgroundColor: .white, size: 35), for: .normal)
        searchBar.tintColor = .black
        navigationItem.titleView = searchBar
                        
        // speech button
        voiceSearchButton.setImage(UIImage(named: "mic")!.kf.offsetingSize(x: -5, y: -5), for: .normal)
        voiceSearchButton.contentMode = .center
        voiceSearchButton.contentEdgeInsets = .init(top: 5, left: 5, bottom: 5, right: 5)
        voiceSearchButton.addTarget(self, action: #selector(onVoiceSearchButton), for: .touchUpInside)
        
        // close button
        closeButton.setImage(UIImage(named: "cancel")!.kf.offsetingSize(x: -10, y: -10), for: .normal)
        closeButton.contentMode = .center
        closeButton.contentEdgeInsets = .init(top: 5, left: 5, bottom: 5, right: 5)
        closeButton.addTarget(self, action: #selector(onCancelButton), for: .touchUpInside)
        
        // bar button items
        let barButtonsStackView = UIStackView()
        barButtonsStackView.axis = .horizontal
        barButtonsStackView.spacing = 20
        barButtonsStackView.addArrangedSubview(voiceSearchButton)
        barButtonsStackView.addArrangedSubview(closeButton)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButtonsStackView)
        
        navigationItem.hidesBackButton = true
        
        // query hints tableView
        tableView.isHidden = true
        view.addSubview(tableView)
        queryHintsAdapter.delegate = self
        
        // empty state label
        emptyStateLabel.font = UIFont.boldSystemFont(ofSize: 17)
        emptyStateLabel.textColor = UIColor(white: 0.7, alpha: 1)
        emptyStateLabel.numberOfLines = 0
        emptyStateLabel.textAlignment = .center
        emptyStateLabel.text = "EmptyStateForEmptyQueryLabel".localized()
        view.addSubview(emptyStateLabel)

        // related articles stack view
        relatedArticlesStackView.axis = .vertical
        relatedArticlesStackView.spacing = 10
        view.addSubview(relatedArticlesStackView)
        
        // related articles title label
        relatedArticlesTitleLabel.text = "Artigos relacionados"
        relatedArticlesTitleLabel.font = UIFont.systemFont(ofSize: 14)
        relatedArticlesTitleLabel.textColor = .darkGray
        relatedArticlesTitleLabel.textAlignment = .left
        relatedArticlesTitleLabel.padding = .init(top: 0, left: 14, bottom: 0, right: 0)
        relatedArticlesTitleLabel.isHidden = true
        relatedArticlesStackView.addArrangedSubview(relatedArticlesTitleLabel)
        
        // related articles collectionView
        collectionView.isHidden = true
        relatedArticlesStackView.addArrangedSubview(collectionView)
        relatedArticlesAdapter.delegate = self
    }
    
    override func layoutViews() {
        super.layoutViews()
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(relatedArticlesStackView.snp.top).offset(-12)
        }
        
        collectionView.snp.makeConstraints {
            $0.height.equalTo(relatedArticlesAdapter.itemSize.height)
        }
        
        emptyStateLabel.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(32)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        
        relatedArticlesStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            keyboardHandlerAdapter.bottomConstraint = $0.bottom.equalTo(view.snp.bottomMargin).offset(-12).constraint.layoutConstraints.first
        }
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.openUrl.observe { [weak self] _ in
            self?.onViewModelOpenUrlChanged()
        }
        
        viewModel.isVoiceRecording.observe { [weak self] _ in
            self?.onViewModelIsVoiceRecordingChanged()
        }
        
        viewModel.queryHints.observe { [weak self] _ in
            self?.onViewModelQueryHintsChanged()
        }
        
        viewModel.relatedArticles.observe { [weak self] _ in
            self?.onViewModelRelatedArticlesChanged()
        }
        
        viewModel.searchText.observe { [weak self] _ in
            self?.onViewModelSearchTextChanged()
        }
    }
    
    // MARK: - ViewModel
    private func onViewModelOpenUrlChanged() {
        guard let url = viewModel.openUrl.value else { return }
        searchBar.endEditing(true)
        navigationController?.pushViewController(WebPageViewController(url: url), animated: true)
    }
    
    private func onViewModelIsVoiceRecordingChanged() {
        let barTintColor = navigationController?.navigationBar.barTintColor ?? .purple
        let tintColor = navigationController?.navigationBar.tintColor ?? .white
        
        if viewModel.isVoiceRecording.value {
            let isRecordingBackgroundImage = UIImage.rounded(backgroundColor: tintColor, size: voiceSearchButton.frame.size.height)
            voiceSearchButton.tintColor = barTintColor
            voiceSearchButton.setBackgroundImage(isRecordingBackgroundImage, for: .normal)
        } else {
            voiceSearchButton.tintColor = tintColor
            voiceSearchButton.setBackgroundImage(nil, for: .normal)
        }
    }
    
    private func onViewModelSearchTextChanged() {
        searchBar.text = viewModel.searchText.value
    }
    
    private func onViewModelQueryHintsChanged() {
        switch viewModel.queryHints.value {
        case .loading(_):
            showQueryHintLoadingState(show: true)
        case .loaded(let data):
            queryHintsAdapter.reload(with: data)
            showQueryHintEmptyState(show: data.isEmpty)
        case .failure(let error),
             .partialFailure(let error, _):
            showQueryHintErrorState(show: true, message: error)
        default: break
        }
    }
    
    private func onViewModelRelatedArticlesChanged() {
        switch viewModel.relatedArticles.value {
        case .loaded(let data):
            relatedArticlesAdapter.reload(with: data)
            showRelatedArticlesEmptyState(show: data.isEmpty)
        default: break
        }
    }
    
    private func showQueryHintLoadingState(show: Bool) {
        if show {
            loadingView.show(in: view, animated: true)
        } else {
            loadingView.dismiss(animated: true)
        }
    }
    
    private func showQueryHintEmptyState(show: Bool) {
        showQueryHintLoadingState(show: false)
        tableView.isHidden = show
        emptyStateLabel.isHidden = !show
        
        if let text = searchBar.text, !text.isEmpty {
            emptyStateLabel.text = "EmptyStateForNotFoundQueryLabel".localized(text)
        } else {
            emptyStateLabel.text = "EmptyStateForEmptyQueryLabel".localized()
        }
    }
    
    private func showQueryHintErrorState(show: Bool, message: String) {
        showQueryHintLoadingState(show: false)
        showAlert(message: message)
    }
    
    private func showRelatedArticlesEmptyState(show: Bool) {
        relatedArticlesTitleLabel.isHidden = show
        collectionView.isHidden = show
    }
    
    // MARK: - SearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.search(query: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        viewModel.search(query: searchBar.text ?? "")
    }
    
    // MARK: - QueryHintTableAdapterDelegate
    func onSelectQueryHint(_ adapter: QueryHintAdapterProtocol, hint: String, at index: Int) {
        viewModel.selectQueryHint(query: hint)
    }
    
    func onDisplayLastQueryHint(_ adapter: QueryHintAdapterProtocol) {
        viewModel.getMoreHints()
    }
    
    // MARK: - RelatedArticlesAdapterDelegate
    func onSelectedRelatedArticle(_ adapter: RelatedArticlesAdapterProtocol, _ relatedArticle: SearchViewModel.RelatedArticle, at index: Int) {
        viewModel.selectRelatedArticle(relatedArticle: relatedArticle)
    }
    
    func onDisplayLastRelatedArticle(_ adapter: RelatedArticlesAdapterProtocol) {
        viewModel.getMoreRelatedArticles()
    }
    
    // MARK: - User actions
    @objc private func onCancelButton() {
        searchBar.endEditing(true)
        
        if let viewControllers = navigationController?.viewControllers.count,
           viewControllers > 1 {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc private func onVoiceSearchButton() {
        viewModel.toggleVoiceRecording()
    }
}
