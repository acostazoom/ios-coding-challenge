
import Foundation

protocol SearchViewModelProtocol: BaseViewModelProtocol {
    var isVoiceRecording: ObservableData<Bool> { get }
    var searchText: ObservableData<String?> { get }
    var queryHints: ObservableState<[String], String> { get }
    var relatedArticles: ObservableState<[SearchViewModel.RelatedArticle], String> { get }
    var openUrl: ObservableData<URL?> { get }
    
    func selectQueryHint(query: String)
    func selectRelatedArticle(relatedArticle: SearchViewModel.RelatedArticle)
    func search(query: String)
    func getMoreHints()
    func getMoreRelatedArticles()
    func toggleVoiceRecording()
}

class SearchViewModel: BaseViewModel,
                       SearchViewModelProtocol,
                       SpeechRecognizerControllerDelegate {
        
    
    private let speechRecognizerController: SpeechRecognizerControllerProtocol
    private let queryRepository: QueryHintsRepositoryProtocol
    private let relatedArticlesRepository: RelatedArticlesRepositoryProtocol
    
    private let queryHintsPaginationController = DataPaginationController<String, String>()
    private let relatedArticlesPaginationController = DataPaginationController<RelatedArticle, String>()
    
    let isVoiceRecording = ObservableData<Bool>(false)
    let searchText = ObservableData<String?>(nil)
    let openUrl = ObservableData<URL?>(nil)
    var queryHints: ObservableState<[String], String> { queryHintsPaginationController.state }
    var relatedArticles: ObservableState<[SearchViewModel.RelatedArticle], String> { relatedArticlesPaginationController.state }
    
    
    init(queryRepository: QueryHintsRepositoryProtocol,
         relatedArticlesRepository: RelatedArticlesRepositoryProtocol,
         speechRecognizerController: SpeechRecognizerControllerProtocol) {
        
        self.queryRepository = queryRepository
        self.relatedArticlesRepository = relatedArticlesRepository
        self.speechRecognizerController = speechRecognizerController
        
        super.init()
        
        speechRecognizerController.delegate = self
        setupPaginationRequests()
    }
    
    func toggleVoiceRecording() {
        if isVoiceRecording.value {
            speechRecognizerController.cancelRecording()
        } else {
            speechRecognizerController.startRecording()
        }
    }
    
    func search(query: String) {
        guard query != searchText.value else { return }
        searchText.value = query
        queryHintsPaginationController.getFirstPage()
        relatedArticlesPaginationController.getFirstPage()
    }
    
    func selectQueryHint(query: String) {
        guard let formattedQuery = query.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else { return }
        openUrl.value = URL(string: Environment.shared.domains.webPageSearchPath(query: formattedQuery))
    }
    
    func selectRelatedArticle(relatedArticle: SearchViewModel.RelatedArticle) {
        openUrl.value = relatedArticle.contentUrl
    }
    
    func getMoreHints() {
        queryHintsPaginationController.getNextPage()
    }
    
    func getMoreRelatedArticles() {        
        relatedArticlesPaginationController.getNextPage()
    }
    
    private func setupPaginationRequests() {
        // QueryHints
        queryHintsPaginationController.getPageFunction = { [weak self] page, callback in
            guard let query = self?.searchText.value,
                  !query.isEmpty else { return callback(.cancelled) }
            
            self?.queryRepository.getQueryHints(query: query, page: page) { result in
                switch result {
                case .success(let hints):
                    let data = hints.result.sorted(by: { $0.popularity > $1.popularity }).map({ $0.query })
                    callback(.success(.init(result: data, currentPage: page, maxPages: hints.maxPages)))
                case .failure(let failure):
                    callback(.error(failure.message))
                }
            }
        }
        
        // RelatedProducts
        relatedArticlesPaginationController.getPageFunction = { [weak self] page, callback in
            guard let query = self?.searchText.value,
                  !query.isEmpty else { return callback(.cancelled) }
            
            self?.relatedArticlesRepository.getRelatedArticles(query: query, page: page) { result in
                switch result {
                case .success(let products):
                    let data = products.result.map({ RelatedArticle(title: $0.title, imageUrl: URL(string: $0.imageUrl), contentUrl: URL(string: $0.url)) })
                    callback(.success(.init(result: data, currentPage: products.currentPage, maxPages: products.maxPages)))
                case .failure(let failure):
                    callback(.error(failure.message))
                }
            }
        }
    }
    
    // MARK: - SpeechRecognizerControllerDelegate
    func onSpeechRecognizerChangedIsRecording(_ isRecording: Bool) {
        isVoiceRecording.value = isRecording
    }
    
    func onSpeechRecognizerParsedText(_ text: String) {
        guard isVoiceRecording.value else { return }
        search(query: text)
    }
    
    func onSpeechRecognizerFailure(_ errorMessage: String) {
        NSLog("onSpeechRecognizerFailure: \(errorMessage)")
    }

    // MARK: - Inner types
    struct RelatedArticle: Equatable {
        let title: String
        let imageUrl: URL?
        let contentUrl: URL?        
    }
    
    struct Factory {
        func getInstance() -> SearchViewModel {
            return SearchViewModel(
                queryRepository: QueryHintsApiRepository(),
                relatedArticlesRepository: RelatedArticlesApiRepository(),
                speechRecognizerController: SpeechRecognizerController()
            )
        }
    }
}
