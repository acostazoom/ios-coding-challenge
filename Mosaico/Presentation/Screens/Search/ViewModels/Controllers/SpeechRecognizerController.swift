
import AVFoundation
import Speech

protocol SpeechRecognizerControllerDelegate: AnyObject {
    func onSpeechRecognizerChangedIsRecording(_ isRecording: Bool)
    func onSpeechRecognizerParsedText(_ text: String)
    func onSpeechRecognizerFailure(_ errorMessage: String)
}

protocol SpeechRecognizerControllerProtocol: AnyObject {
    var delegate: SpeechRecognizerControllerDelegate? { get set }
    var isRecording: Bool { get }
    
    func cancelRecording()
    func startRecording()
}

class SpeechRecognizerController: SpeechRecognizerControllerProtocol {
    private let audioEngine = AVAudioEngine()
    private let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    private let request = SFSpeechAudioBufferRecognitionRequest()
    private var recognitionTask: SFSpeechRecognitionTask?
    private var autoStopRecognition: Timer?
    
    private(set) var isRecording = false {
        didSet {
            delegate?.onSpeechRecognizerChangedIsRecording(isRecording)
        }
    }
    
    weak var delegate: SpeechRecognizerControllerDelegate?
        
    func cancelRecording() {
        recognitionTask?.finish()
        recognitionTask = nil
        
        autoStopRecognition?.invalidate()
        autoStopRecognition = nil
        
        request.endAudio()
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        
        isRecording = false
    }
    
    func startRecording() {
        requestSpeechAuthorization { [weak self] authorized in
            if authorized {
                self?.recordAndRecognizeSpeech()
            }
        }
    }
    
    private func recordAndRecognizeSpeech() {
        isRecording = true
        
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            dispatchError(message: "AudioEngineGenericError".localized())
            NSLog(error.localizedDescription)
            return
        }
        
        guard let myRecognizer = SFSpeechRecognizer() else {
            dispatchError(message: "SpeechRecognizerLocaleError".localized())
            return
        }
        guard myRecognizer.isAvailable else {
            dispatchError(message: "SpeechRecognizerAvailabilityError".localized())
            return
        }
        
        resetAutoStopTimer()
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { [weak self] result, error in
            if let result = result {
                let bestString = result.bestTranscription.formattedString
                self?.dispatchParsedText(text: bestString)
            } else if let error = error {
                self?.dispatchError(message: "SpeechRecognizerGenericError".localized())
                NSLog(error.localizedDescription)
            }
        })
    }
    
    private func requestSpeechAuthorization(_ completion: @escaping (Bool) -> Void) {
        SFSpeechRecognizer.requestAuthorization { [weak self] authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    completion(true)
                case .denied,
                     .restricted,
                     .notDetermined:
                    self?.dispatchError(message: "SpeechRecognizerPermissionDenied".localized())
                    completion(false)                
                @unknown default:
                    self?.dispatchError(message: "SpeechRecognizerPermissionDenied".localized())
                    completion(false)
                }
            }
        }
    }
    
    private func resetAutoStopTimer() {
        autoStopRecognition?.invalidate()
        
        guard isRecording else {
            autoStopRecognition = nil
            return
        }
        
        autoStopRecognition = Timer.scheduledTimer(withTimeInterval: 3, repeats : false, block: { [weak self] _ in
            self?.cancelRecording()
        })
    }
    
    private func dispatchError(message: String) {
        cancelRecording()
        delegate?.onSpeechRecognizerFailure(message)
    }
    
    private func dispatchParsedText(text: String) {
        resetAutoStopTimer()
        delegate?.onSpeechRecognizerParsedText(text)
    }
}
