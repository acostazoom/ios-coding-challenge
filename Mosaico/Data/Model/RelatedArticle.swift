
import Foundation

struct RelatedArticle {
    let id: String
    let title: String
    let url: String
    let imageUrl: String
}
