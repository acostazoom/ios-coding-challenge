
import Foundation

struct QueryHint {
    let query: String
    let popularity: Int
}
