
import Foundation
import Alamofire

class QueryHintsApiRepository: BaseApiRepository,
                               QueryHintsRepositoryProtocol {
    private var getQueryHintsRequest: DataRequest?
    
    func getQueryHints(query: String, page: Int, _ completion: @escaping (Result<RequestPageResult<[QueryHint]>, Failure>) -> Void) {
        getQueryHintsRequest?.cancel()
        
        let request = AF.request(
            searchApiDomain.appending("/1/indexes/prod_items_query_suggestion"),
            parameters: apiSecretParams + ["query": query, "page": page]
        )
        
        request.response { response in
            guard !request.isCancelled else { return }
            
            if let data = QueryHintsResponse.decode(response.data) {
                let hints: [QueryHint] = data.hits?.map { hit in
                    return QueryHint(query: hit.query, popularity: hit.popularity)
                } ?? []
                let pageResult = RequestPageResult<[QueryHint]>(result: hints, currentPage: data.page, maxPages: data.nbPages)
                completion(.success(pageResult))
            } else if let networkError = response.error {
                completion(.failure(.networkError(networkError.localizedDescription)))
            } else {
                completion(.failure(.genericError("RequestGenericError".localized())))
            }
        }
        
        getQueryHintsRequest = request
    }
    
    
    // MARK: - Inner types
    fileprivate struct QueryHintsResponse: Codable {
        let hits: [Hit]?
        let nbHits, page, nbPages, hitsPerPage: Int
        let exhaustiveNbHits: Bool
        let query: String?
        let params: String?
        let processingTimeMS: Int
    }

    fileprivate struct Hit: Codable {
        let nbWords, popularity: Int
        let prodItems: ProdItems
        let query, objectID: String
        let highlightResult: HighlightResult

        enum CodingKeys: String, CodingKey {
            case nbWords = "nb_words"
            case popularity
            case prodItems = "prod_items"
            case query, objectID
            case highlightResult = "_highlightResult"
        }
    }

    fileprivate struct HighlightResult: Codable {
        let query: QueryData
    }

    fileprivate struct QueryData: Codable {
        let value: String
        let matchLevel: String?
        let fullyHighlighted: Bool
        let matchedWords: [String]?
    }
    
    fileprivate struct ProdItems: Codable {
        let exactNbHits: Int
        let facets: Facets?

        enum CodingKeys: String, CodingKey {
            case exactNbHits = "exact_nb_hits"
            case facets
        }
    }

    fileprivate struct Facets: Codable {
        let exactMatches, analytics: Analytics?

        enum CodingKeys: String, CodingKey {
            case exactMatches = "exact_matches"
            case analytics
        }
    }

    fileprivate struct Analytics: Codable {
        let categoryName: [CategoryName]?
    }

    fileprivate struct CategoryName: Codable {
        let value: String?
        let count: Int
    }
    
}
