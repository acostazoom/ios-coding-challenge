
import Foundation

protocol QueryHintsRepositoryProtocol: AnyObject {
    func getQueryHints(query: String, page: Int, _ completion: @escaping (Result<RequestPageResult<[QueryHint]>, Failure>) -> Void)
}
