
import Foundation
import Alamofire

class RelatedArticlesApiRepository: BaseApiRepository,
                                    RelatedArticlesRepositoryProtocol {
    private var getRelatedProductsRequest: DataRequest?
    
    func getRelatedArticles(query: String, page: Int, _ completion: @escaping (Result<RequestPageResult<[RelatedArticle]>, Failure>) -> Void) {
        getRelatedProductsRequest?.cancel()
        
        let request = AF.request(
            searchApiDomain.appending("/1/indexes/prod_article_items"),
            parameters: ["query": query, "hitsPerPage": 6, "page": page] + apiSecretParams
        )
        
        request.response { response in
            guard !request.isCancelled else { return }
            
            if let data = RelatedProductsResponse.decode(response.data) {
                let relatedProducts: [RelatedArticle] = data.hits?.map { hit in
                    return RelatedArticle(
                        id: hit.id ?? "",
                        title: hit.title ?? "",
                        url: hit.url ?? "",
                        imageUrl: hit.imageURL ?? ""
                    )
                } ?? []
                let pageResult = RequestPageResult<[RelatedArticle]>(result: relatedProducts, currentPage: data.page, maxPages: data.nbPages)
                completion(.success(pageResult))
            } else if let networkError = response.error {
                completion(.failure(.networkError(networkError.localizedDescription)))
            } else {
                completion(.failure(.genericError("RequestGenericError".localized())))
            }
        }
        
        getRelatedProductsRequest = request
    }
    
    // MARK: - Inner types
    fileprivate struct RelatedProductsResponse: Codable {
        let hits: [Hit]?
        let nbHits, page, nbPages, hitsPerPage: Int
        let exhaustiveNbHits: Bool
        let query: String?
        let params: String?
        let processingTimeMS: Int
    }
    
    fileprivate struct Hit: Codable {
        let id, title, subtitle: String?
        let url: String?
        let imageURL: String?
        let timestamp: String?
        let tags: [String]?
        let objectID: String?
        let highlightResult: HighlightResult?

        enum CodingKeys: String, CodingKey {
            case id, title, subtitle, url
            case imageURL = "imageUrl"
            case timestamp, tags, objectID
            case highlightResult = "_highlightResult"
        }
    }

    fileprivate struct HighlightResult: Codable {
        let title, subtitle, url: Subtitle?
        let tags: [Subtitle]?
    }

    fileprivate struct Subtitle: Codable {
        let value: String
        let matchLevel: String?
        let fullyHighlighted: Bool?
        let matchedWords: [String]?
    }
}
