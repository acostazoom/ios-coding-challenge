
import Foundation

protocol RelatedArticlesRepositoryProtocol: AnyObject {
    func getRelatedArticles(query: String, page: Int, _ completion: @escaping (Result<RequestPageResult<[RelatedArticle]>, Failure>) -> Void)
}
