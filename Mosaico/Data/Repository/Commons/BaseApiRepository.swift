
import Foundation

class BaseApiRepository {
    var searchApiDomain: String { Environment.shared.domains.searchApiDomain }
    var apiSecretParams: [String : String] { ["x-algolia-api-key": "6142f94b81af845c61ae58dbe1d42f5c", "x-algolia-application-id": "8SYDLMHHJG"] }
}
