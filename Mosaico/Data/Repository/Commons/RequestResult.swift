
import Foundation

enum Failure: Error {
    case genericError(_ message: String)
    case networkError(_ message: String)
    
    var message: String {
        switch self {
        case .genericError(let message),
             .networkError(let message): return message
        }
    }
    
    var localizedDescription: String {
        return message
    }
}

struct RequestPageResult<T> {
    let result: T
    let currentPage: Int
    let maxPages: Int
}
