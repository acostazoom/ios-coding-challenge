
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupInitialScreen()
        setupAppearance()
        return true
    }

}

// MARK: - Application's UI
extension AppDelegate {
    private func setupInitialScreen() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        window.rootViewController = UINavigationController(rootViewController: SearchViewController())
        window.makeKeyAndVisible()
    }
    
    private func setupAppearance() {
        UINavigationBar.appearance().barTintColor = .purple
        UINavigationBar.appearance().tintColor = .white
    }
}

