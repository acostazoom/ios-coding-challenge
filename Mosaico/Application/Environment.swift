//
//  Environment.swift
//  Mosaico
//
//  Created by Jorge Luis on 20/05/21.
//

import Foundation

protocol EnvironmentProtocol {
    var domains: Environment.Domains { get }
}

class Environment: EnvironmentProtocol {
    static let shared: EnvironmentProtocol = Environment()
    
    let domains: Domains = .init(
        searchApiDomain: "https://search-api.zoom.com.br",
        webPageDomain: "https://www.zoom.com.br"
    )

    private init() {}
    
    // MARK: - InnerTypes
    struct Domains {
        let searchApiDomain: String
        let webPageDomain: String
        
        func webPageSearchPath(query: String) -> String {
            return "\(webPageDomain)/search?q=\(query)"
        }
    }
}
