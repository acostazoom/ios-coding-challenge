Desafio de codificação IOS

Bem vindo à etapa de desafio técnico do Zoom & Buscapé.
Se você chegou até aqui é por que acreditamos em você, e queremos entender um mais sobre o seu perfil.

O seu desafio será desenvolver um pequeno app onde o usuário pesquise por produtos, marcas e conteúdos.
Ao clicar sobre algum conteúdo o usuário deverá ser redirecionado para uma webview onde existirão artigos relacionados a query de busca imputadas pelo usuário.

Visualmente o seu app pode se assemelhar com o seguinte, mas sinta-se livre para trazer as suas próprias contribuições visuais para o projeto.

   
          


Para listar as sugestões você deverá usar o seguinte endpoint:

https://search-api.zoom.com.br/1/indexes/prod_items_query_suggestion?query=iph&x-algolia-api-key=6142f94b81af845c61ae58dbe1d42f5c&x-algolia-application-id=8SYDLMHHJG

Se o usuário clicar sobre uma das sugestões você deverá direcioná-lo para uma webview com a seguinte url onde "q" deve ser o termo no qual o usuário clicou.
https://www.zoom.com.br/search?q=iphone

E para listar os artigos relacionados a query do usuário você deverá usar o seguinte endpoint:



Para listar os artigos relacionados a query do usuário você deverá usar o seguinte endpoint:

https://search-api.zoom.com.br/1/indexes/prod_article_items?query=ipho&x-algolia-api-key=6142f94b81af845c61ae58dbe1d42f5c&x-algolia-application-id=8SYDLMHHJG&hitsPerPage=6

Se o usuário clicar sobre uma das sugestões você deverá direcioná-lo para uma webview com a seguinte url especificada no parâmetro "url"  do endpoint acima .




Seu projeto deverá ser desenvolvido no seguinte repositório.


git clone git@bitbucket.org:acostazoom/ios-coding-challenge.git

Crie uma Branch com o seu nome. Tente fazer commits regulares.
Tente dividir o seu projeto em etapas, você terá 5 dias para concluir o projeto.
No seu projeto será avaliado de acordo com os seguintes critérios:
- Arquitetura 
- Modularização
- Reusabilidade
- Testes