
import XCTest

class MosaicoUITests: XCTestCase {
    var app: XCUIApplication { XCUIApplication() }
    
    override func setUp() {
        continueAfterFailure = false
        app.launch()
    }

    func test_tableViewAndCollectionViewChangedWhenSearchTerm()  {
        // Given
        let searchBar = app.navigationBars.searchFields.element
        let originalTableViewElements = app.tables.children(matching: .cell).count
        let originalCollectionViewElements = app.collectionViews.children(matching: .cell).count
        
        // When
        searchBar.tap()
        searchBar.typeText("sony")

        // Then
        _ = app.tables.children(matching: .cell).element.waitForExistence(timeout: 5)
        XCTAssertNotEqual(originalTableViewElements, app.tables.children(matching: .cell).count)
        
        _ = app.collectionViews.children(matching: .cell).element.waitForExistence(timeout: 5)
        XCTAssertNotEqual(originalCollectionViewElements, app.collectionViews.children(matching: .cell).count)
    }

}
