
import UIKit
import XCTest

extension XCTestCase {
    func makeReadyForTests(sut: UIViewController) {        
        let root = UINavigationController()
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = root
        window.makeKeyAndVisible()
        
        let exp = expectation(description: "wait for viewcontroller under test lifecycle")
        
        sut.onViewDidAppear { exp.fulfill() }
        root.pushViewController(sut, animated: false)
        
        wait(for: [exp], timeout: 3)
    }
}

extension UIViewController {
    private func getObserver() -> ObserverViewController {
        if let observer = self.children.first(where: { $0 is ObserverViewController }) as? ObserverViewController {
            return observer
        }
        let observer = ObserverViewController()
        
        observer.willMove(toParent: self)
        self.addChild(observer)
        self.view.addSubview(observer.view)
        observer.didMove(toParent: self)
        return observer
    }
    
    func onViewDidAppear(_ handler: @escaping (() -> Void)) {
        getObserver().onDidAppear = handler
    }
}

fileprivate class ObserverViewController: UIViewController {
    var onDidAppear: (() -> Void)?
    
    override func loadView() {
        view = UIView()
        view.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        onDidAppear?()
    }
}
