
import XCTest
@testable import Mosaico

class SearchViewModelTests: XCTestCase {
    var queryRepository: SearchMocks.QueryHintsRepository!
    var relatedArticlesRepository: SearchMocks.RelatedArticlesRespository!
    var speechController: SearchMocks.SpeechRecognizerController!
    
    var sut: SearchViewModel!
    
    override func setUp() {
        queryRepository = .init()
        relatedArticlesRepository = .init()
        speechController = .init()
        
        sut = SearchViewModel(
            queryRepository: queryRepository,
            relatedArticlesRepository: relatedArticlesRepository,
            speechRecognizerController: speechController
        )
    }    
    
    class TestVC: UIViewController {
        var didLoad: Bool = false
        var willAppear: Bool = false
        var didAppear: Bool = false
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            didLoad = true
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            didAppear = true
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            willAppear = true
        }
    }

    
    // MARK: - State setters
    @discardableResult
    func setQueryHintRepositoryValidState(page: Int = 0, maxPages: Int = 1) -> [String] {
        let result: [QueryHint] = [
            .init(query: "mockada\(page)", popularity: 3),
            .init(query: "mock\(page)", popularity: 2),
            .init(query: "mk\(page)", popularity: 1)
        ]
        let shuffledResult = result.shuffled()
        queryRepository.resultPages.insert(.success(.init(result: shuffledResult, currentPage: page, maxPages: maxPages)), at: page)
        
        return result.map({ $0.query })
    }
    
    @discardableResult
    func setRelatedArticlesRepositoryValidState(page: Int = 0, maxPages: Int = 1) -> [SearchViewModel.RelatedArticle] {
        let result: [RelatedArticle] = [
            RelatedArticle(id: "a\(page)", title: "MockA\(page)", url: "http://mock\(page)", imageUrl: "http://mock_image\(page)"),
            RelatedArticle(id: "b\(page)", title: "MockB\(page)", url: "http://mock\(page)", imageUrl: "http://mock_image\(page)"),
            RelatedArticle(id: "c\(page)", title: "MockC\(page)", url: "http://mock\(page)", imageUrl: "http://mock_image\(page)"),
        ]
        relatedArticlesRepository.resultPages.insert(.success(.init(result: result, currentPage: page, maxPages: maxPages)), at: page)
        
        return result.map({ SearchViewModel.RelatedArticle(title: $0.title, imageUrl: URL(string: $0.imageUrl), contentUrl: URL(string: $0.url))})
    }
}

// MARK: - OpenUrl Tests
extension SearchViewModelTests {
    func test_openUrlFromSelectedRelatedArticle() {
        // Given
        let articleUrl = "http://mock_url"
        let mockArticle = SearchViewModel.RelatedArticle(title: "", imageUrl: nil, contentUrl: URL(string: articleUrl))
        
        // When
        sut.selectRelatedArticle(relatedArticle: mockArticle)
        
        // Then
        XCTAssertEqual(sut.openUrl.value?.absoluteString, articleUrl)
    }
    
    func test_openUrlFromSelectedQueryHint() {
        // Given
        let queryHint = "mock hint"
        
        // When
        sut.selectQueryHint(query: queryHint)
        
        // Then
        XCTAssertEqual(sut.openUrl.value?.absoluteString, "https://www.zoom.com.br/search?q=mock%20hint")
    }
    
}

// MARK: - QueryHints Tests
extension SearchViewModelTests {
    
    func test_getHintsFromEmptyQuery() {
        // Given
        let query = ""
        setQueryHintRepositoryValidState()
        
        // When
        sut.search(query: query)
        
        // Then
        XCTAssertEqual(sut.queryHints.data, [])
    }
    
    func test_getHintsFromValidQuery() {
        // Given
        let query = "mock query"
        let expectedResult = setQueryHintRepositoryValidState()
        
        // When
        sut.search(query: query)
        
        // Then
        XCTAssertEqual(sut.queryHints.data, expectedResult)
    }
    
    func test_getTwoPagesOfHintsFromValidQuery() {
        // Given
        let query = "mock query"
        let firstPageExpectedResult = setQueryHintRepositoryValidState(page: 0, maxPages: 2)
        let secondPageExpectedResult = setQueryHintRepositoryValidState(page: 1, maxPages: 2)
        
        // When
        sut.search(query: query)
        sut.getMoreHints()
        
        // Then
        XCTAssertEqual(sut.queryHints.data, firstPageExpectedResult + secondPageExpectedResult)
    }
    
    func test_getHintsObservableStateFlowFromValidQuery() {
        // Given
        let query = "mock query"
        let expectedData = setQueryHintRepositoryValidState()
        var expectedStateFlow: [ViewModelDataState<[String], String>] = [.notLoaded, .loading(data: nil), .loaded(data: expectedData)]
        
        // When
        defer {
            sut.search(query: query)
            XCTAssert(expectedStateFlow.isEmpty)
        }
        
        // Then
        XCTAssertEqual(sut.queryHints.value, expectedStateFlow.removeFirst())
        sut.queryHints.observe { result in
            XCTAssertEqual(result, expectedStateFlow.removeFirst())
        }
    }
    
    func test_getTwoPagesOfHintsObservableStateFlowFromValidQuery() {
        // Given
        let query = "mock query"
        let firstPageExpectedData = setQueryHintRepositoryValidState(page: 0, maxPages: 2)
        let secondPageExpectedData = setQueryHintRepositoryValidState(page: 1, maxPages: 2)
        var expectedStateFlow: [ViewModelDataState<[String], String>] = [
            .notLoaded,
            .loading(data: nil),
            .loaded(data: firstPageExpectedData),
            .loading(data: firstPageExpectedData),
            .loaded(data: firstPageExpectedData + secondPageExpectedData)
        ]
        
        // When
        defer {
            sut.search(query: query)
            sut.getMoreHints()
            XCTAssert(expectedStateFlow.isEmpty)
        }
        
        // Then
        XCTAssertEqual(sut.queryHints.value, expectedStateFlow.removeFirst())
        sut.queryHints.observe { result in
            XCTAssertEqual(result, expectedStateFlow.removeFirst())
        }
    }
        
}

// MARK: - SpeechRecognition Tests
extension SearchViewModelTests {
    func test_speechRecognitionToggleIsVoiceRecordingFlowState() {
        // Given
        var expectedStateFlow = [false, true, false]
        
        // When
        defer {
            sut.toggleVoiceRecording()
            sut.toggleVoiceRecording()
            XCTAssert(expectedStateFlow.isEmpty)
        }
        
        // Then
        XCTAssertEqual(sut.isVoiceRecording.value, expectedStateFlow.removeFirst())
        sut.isVoiceRecording.observe { isRecording in
            XCTAssertEqual(isRecording, expectedStateFlow.removeFirst())
        }
    }
    
    func test_speechRecognitionRecognizeSpeechFlowState() {
        // Given
        var expectedStateFlow = ["te", "test", "test mock"]
        speechController.isRecording = true
        
        // When
        defer {
            speechController.recognizedSpeech = "te"
            speechController.recognizedSpeech = "test"
            speechController.recognizedSpeech = "test mock"
            XCTAssert(expectedStateFlow.isEmpty)
        }
        
        // Then
        sut.searchText.observe { recognizedSpeech in
            XCTAssertEqual(recognizedSpeech, expectedStateFlow.removeFirst())
        }
    }
}

// MARK: - RelatedArticles Tests
extension SearchViewModelTests {
    func test_getRelatedArticlesFromEmptyQuery() {
        // Given
        let query = ""
        setRelatedArticlesRepositoryValidState()
        
        // When
        sut.search(query: query)
        
        // Then
        XCTAssertEqual(sut.relatedArticles.data, [])
    }
    
    func test_getRelatedArticlesFromValidQuery() {
        // Given
        let query = "mock query"
        let expectedResult = setRelatedArticlesRepositoryValidState()
        
        // When
        sut.search(query: query)
        
        // Then
        XCTAssertEqual(sut.relatedArticles.data, expectedResult)
    }
    
    func test_getTwoPagesOfRelatedArticlesFromValidQuery() {
        // Given
        let query = "mock query"
        let firstPageExpectedResult = setRelatedArticlesRepositoryValidState(page: 0, maxPages: 2)
        let secondPageExpectedResult = setRelatedArticlesRepositoryValidState(page: 1, maxPages: 2)
        
        // When
        sut.search(query: query)
        sut.getMoreRelatedArticles()
        
        // Then
        XCTAssertEqual(sut.relatedArticles.data, firstPageExpectedResult + secondPageExpectedResult)
    }
    
    func test_getRelatedArticlesObservableStateFlowFromValidQuery() {
        // Given
        let query = "mock query"
        let expectedData = setRelatedArticlesRepositoryValidState()
        var expectedStateFlow: [ViewModelDataState<[SearchViewModel.RelatedArticle], String>] = [
            .notLoaded,
            .loading(data: nil),
            .loaded(data: expectedData)
        ]
        
        // When
        defer {
            sut.search(query: query)
            XCTAssert(expectedStateFlow.isEmpty)
        }
        
        // Then
        XCTAssertEqual(sut.relatedArticles.value, expectedStateFlow.removeFirst())
        sut.relatedArticles.observe { result in
            XCTAssertEqual(result, expectedStateFlow.removeFirst())
        }
    }
    
    func test_getTwoPagesOfRelatedArticlesObservableStateFlowFromValidQuery() {
        // Given
        let query = "mock query"
        let firstPageExpectedData = setRelatedArticlesRepositoryValidState(page: 0, maxPages: 2)
        let secondPageExpectedData = setRelatedArticlesRepositoryValidState(page: 1, maxPages: 2)
        var expectedStateFlow: [ViewModelDataState<[SearchViewModel.RelatedArticle], String>] = [
            .notLoaded,
            .loading(data: nil),
            .loaded(data: firstPageExpectedData),
            .loading(data: firstPageExpectedData),
            .loaded(data: firstPageExpectedData + secondPageExpectedData)
        ]
        
        // When
        defer {
            sut.search(query: query)
            sut.getMoreRelatedArticles()
            XCTAssert(expectedStateFlow.isEmpty)
        }
        
        // Then
        XCTAssertEqual(sut.relatedArticles.value, expectedStateFlow.removeFirst())
        sut.relatedArticles.observe { result in
            XCTAssertEqual(result, expectedStateFlow.removeFirst())
        }
    }
    
}
