
import XCTest
@testable import Mosaico

class SearchViewControllerTests: XCTestCase {
    var viewModel: SearchMocks.ViewModel!
    
    var sut: SearchViewController!
    
    override func setUp() {
        viewModel = .init()
        
        sut = SearchViewController(viewModel)
                    
        makeReadyForTests(sut: sut)
    }

    func test_changeVoiceSearchButtonColorWhenIsRecordingChanges() {
        func testForIsRecording(expectedIsRecording: Bool) {
            // Given
            let originalButtonColor = sut.voiceSearchButton.tintColor
            let originalButtonBackground = sut.voiceSearchButton.currentBackgroundImage
            
            // When
            viewModel.isVoiceRecording.value = expectedIsRecording
            
            // Then
            XCTAssertNotEqual(originalButtonColor, sut.voiceSearchButton.tintColor)
            XCTAssertNotEqual(originalButtonBackground, sut.voiceSearchButton.currentBackgroundImage)
        }
        
        testForIsRecording(expectedIsRecording: true)
        testForIsRecording(expectedIsRecording: false)
    }
    
    func test_changeSearchBarTextWhenViewModelTextChanges() {
        func testForText(expectedText: String) {
            // Given
            let originalText = sut.searchBar.text
            
            // When
            viewModel.searchText.value = expectedText
            
            // Then
            XCTAssertNotEqual(originalText, sut.searchBar.text)
            XCTAssertEqual(sut.searchBar.text, expectedText)
        }
        
        testForText(expectedText: "test")
        testForText(expectedText: "test two")
    }
    
    func test_changeTableViewWhenViewModelQueriesUpdate() {
        func testForQueries(expectedQueriesResult: [String]) {
            // Given
            let originalCells = sut.tableView.visibleCells.count
            
            // When
            viewModel.queryHints.value = .loaded(data: expectedQueriesResult)
            
            // Then
            XCTAssertNotEqual(originalCells, sut.tableView.visibleCells.count)
            XCTAssertEqual(expectedQueriesResult.count, sut.tableView.visibleCells.count)
            XCTAssertEqual(expectedQueriesResult, sut.tableView.visibleCells.map({ $0.textLabel?.text }))
        }
        
        testForQueries(expectedQueriesResult: ["tes", "te", "do", "bruxo"])
        testForQueries(expectedQueriesResult: ["teste", "do", "mago"])
    }
}

