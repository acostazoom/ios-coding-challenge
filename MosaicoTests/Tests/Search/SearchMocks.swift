
import Foundation
@testable import Mosaico

struct SearchMocks {
    private init() {}
    
    class QueryHintsRepository: QueryHintsRepositoryProtocol {
        var resultPages: [Result<RequestPageResult<[QueryHint]>, Failure>] = []
        
        func getQueryHints(query: String, page: Int, _ completion: @escaping (Result<RequestPageResult<[QueryHint]>, Failure>) -> Void) {
            guard let result = resultPages.get(at: page) else { return }
            completion(result)
        }
    }
    
    class RelatedArticlesRespository: RelatedArticlesRepositoryProtocol {
        var resultPages: [Result<RequestPageResult<[RelatedArticle]>, Failure>] = []
        
        func getRelatedArticles(query: String, page: Int, _ completion: @escaping (Result<RequestPageResult<[RelatedArticle]>, Failure>) -> Void) {
            guard let result = resultPages.get(at: page) else { return }
            completion(result)
        }
    }
    
    class SpeechRecognizerController: SpeechRecognizerControllerProtocol {
        var delegate: SpeechRecognizerControllerDelegate?
        
        var isRecording: Bool = false {
            didSet { delegate?.onSpeechRecognizerChangedIsRecording(isRecording) }
        }
        
        var recognizedSpeech: String = "" {
            didSet { delegate?.onSpeechRecognizerParsedText(recognizedSpeech) }
        }
        
        var recognitionError: String = "" {
            didSet { delegate?.onSpeechRecognizerFailure(recognitionError) }
        }
        
        func cancelRecording() {
            isRecording = false
        }
        
        func startRecording() {
            isRecording = true
        }
    }
    
    class ViewModel: BaseViewModel, SearchViewModelProtocol {
        var isVoiceRecording = ObservableData<Bool>(false)
        
        var searchText = ObservableData<String?>(nil)
        
        var queryHints = ObservableState<[String], String>()
        
        var relatedArticles = ObservableState<[SearchViewModel.RelatedArticle], String>()
        
        var openUrl = ObservableData<URL?>(nil)
        
        func selectQueryHint(query: String) {
            // Mock
        }
        
        func selectRelatedArticle(relatedArticle: SearchViewModel.RelatedArticle) {
            // Mock
        }
        
        func search(query: String) {
            // Mock
        }
        
        func getMoreHints() {
            // Mock
        }
        
        func getMoreRelatedArticles() {
            // Mock
        }
        
        func toggleVoiceRecording() {
            // Mock
        }
    }
}
