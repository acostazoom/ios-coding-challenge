
import XCTest
@testable import Mosaico

class DataPaginationControllerTests: XCTestCase {
    
    func test_getFirstThreePages() {
        // Given
        let sut = DataPaginationController<EquatableData, Never>()
        sut.getPageFunction = { page, callback in
            callback(.success(.init(result: [page], currentPage: page, maxPages: 3)))
        }
        
        var expectedStateFlow: [ViewModelDataState<[EquatableData], Never>] = [
            .notLoaded, // initial state
            .loading(data: nil), // loading first page
            .loaded(data: [0]), // first page loaded
            .loading(data: [0]), // loading second page
            .loaded(data: [0, 1]), // second page loaded
            .loading(data: [0, 1]), // loading third page
            .loaded(data: [0, 1, 2]) // third page loaded
        ]
        
        // When
        defer {
            sut.getFirstPage()
            sut.getNextPage()
            sut.getNextPage()
        }
        
        // Then
        XCTAssertEqual(sut.state.value, expectedStateFlow.removeFirst())
        sut.state.observe { state in
            XCTAssertEqual(state, expectedStateFlow.removeFirst())
            if expectedStateFlow.isEmpty {
                XCTAssertEqual(sut.currentPage, 2)
            }
        }
    }
    
    func test_getFirstThreePagesWithoutCallingGetFirstPage() {
        // Given
        let sut = DataPaginationController<EquatableData, Never>()
        sut.getPageFunction = { page, callback in
            callback(.success(.init(result: [page], currentPage: page, maxPages: 3)))
        }
        
        var expectedStateFlow: [ViewModelDataState<[EquatableData], Never>] = [
            .notLoaded, // initial state
            .loading(data: nil), // loading first page
            .loaded(data: [0]), // first page loaded
            .loading(data: [0]), // loading second page
            .loaded(data: [0, 1]), // second page loaded
            .loading(data: [0, 1]), // loading third page
            .loaded(data: [0, 1, 2]) // third page loaded
        ]
        
        // When
        defer {
            sut.getNextPage()
            sut.getNextPage()
            sut.getNextPage()
        }
        
        // Then
        XCTAssertEqual(sut.state.value, expectedStateFlow.removeFirst())
        sut.state.observe { state in
            XCTAssertEqual(state, expectedStateFlow.removeFirst())
            if expectedStateFlow.isEmpty {
                XCTAssertEqual(sut.currentPage, 2)
            }
        }
    }
    
    func test_getFirstTwoPagesAndThirdPageWithError() {
        // Given
        let sut = DataPaginationController<EquatableData, Int>()
        sut.getPageFunction = { page, callback in
            if page == 2 {
                callback(.error(0))
            } else {
                callback(.success(.init(result: [page], currentPage: page, maxPages: 3)))
            }
        }
        
        var expectedStateFlow: [ViewModelDataState<[EquatableData], Int>] = [
            .notLoaded, // initial state
            .loading(data: nil), // loading first page
            .loaded(data: [0]), // first page loaded
            .loading(data: [0]), // loading second page
            .loaded(data: [0, 1]), // second page loaded
            .loading(data: [0, 1]), // loading third page
            .partialFailure(error: 0, data: [0, 1]) // third page failed to load, but kept the previous valid data.
        ]
        
        // When
        defer {
            sut.getFirstPage()
            sut.getNextPage()
            sut.getNextPage()
        }
        
        // Then
        XCTAssertEqual(sut.state.value, expectedStateFlow.removeFirst())
        sut.state.observe { state in
            XCTAssertEqual(state, expectedStateFlow.removeFirst())
            if expectedStateFlow.isEmpty {
                XCTAssertEqual(sut.currentPage, 1)
            }
        }
    }
    
    func test_getFirstTwoPagesAndThirdPageCancelled() {
        // Given
        let sut = DataPaginationController<EquatableData, Int>()
        sut.getPageFunction = { page, callback in
            if page == 2 {
                callback(.cancelled)
            } else {
                callback(.success(.init(result: [page], currentPage: page, maxPages: 3)))
            }
        }
        
        var expectedStateFlow: [ViewModelDataState<[EquatableData], Int>] = [
            .notLoaded, // initial state
            .loading(data: nil), // loading first page
            .loaded(data: [0]), // first page loaded
            .loading(data: [0]), // loading second page
            .loaded(data: [0, 1]), // second page loaded
            .loading(data: [0, 1]), // loading third page
            .loaded(data: [0, 1]) // third page request was cancelled, so the stated rollbacks to loaded with the previous data.
        ]
        
        // When
        defer {
            sut.getFirstPage()
            sut.getNextPage()
            sut.getNextPage()
        }
        
        // Then
        XCTAssertEqual(sut.state.value, expectedStateFlow.removeFirst())
        sut.state.observe { state in
            XCTAssertEqual(state, expectedStateFlow.removeFirst())
            if expectedStateFlow.isEmpty {
                XCTAssertEqual(sut.currentPage, 1)
            }
        }
    }
    
    func test_getFirstThreePagesFromEquatableDataWithTheSameContent() {
        // Given
        let sut = DataPaginationController<EquatableData, Never>()
        sut.getPageFunction = { page, callback in
            callback(.success(.init(result: [0], currentPage: page, maxPages: 3)))
        }
        
        var expectedStateFlow: [ViewModelDataState<[EquatableData], Never>] = [
            .notLoaded, // initial state
            .loading(data: nil), // loading first page
            .loaded(data: [0]), // first page loaded
            .loading(data: [0]), // loading second page
            .loaded(data: [0]), // second page loaded
            .loading(data: [0]), // loading third page
            .loaded(data: [0]) // third page loaded
        ]
        
        // When
        defer {
            sut.getFirstPage()
            sut.getNextPage()
            sut.getNextPage()
        }
        
        // Then
        XCTAssertEqual(sut.state.value, expectedStateFlow.removeFirst())
        sut.state.observe { state in
            XCTAssertEqual(state, expectedStateFlow.removeFirst())
            if expectedStateFlow.isEmpty {
                XCTAssertEqual(sut.currentPage, 0)
            }
        }
    }
    
    func test_getFirstThreePagesFromNotEquatableDataWithTheSameContent() {
        // Given
        let sut = DataPaginationController<NotEquatableData, Never>()
        sut.getPageFunction = { page, callback in
            callback(.success(.init(result: [0], currentPage: page, maxPages: 3)))
        }
        
        var expectedStateFlow: [ViewModelDataState<[NotEquatableData], Never>] = [
            .notLoaded, // initial state
            .loading(data: nil), // loading first page
            .loaded(data: [0]), // first page loaded
            .loading(data: [0]), // loading second page
            .loaded(data: [0, 0]), // second page loaded
            .loading(data: [0, 0]), // loading third page
            .loaded(data: [0, 0, 0]) // third page loaded
        ]
        
        // When
        defer {
            sut.getFirstPage()
            sut.getNextPage()
            sut.getNextPage()
        }
        
        // Then
        XCTAssertEqual(sut.state.value.equatable, expectedStateFlow.removeFirst().equatable)
        sut.state.observe { state in
            XCTAssertEqual(state.equatable, expectedStateFlow.removeFirst().equatable)
            if expectedStateFlow.isEmpty {
                XCTAssertEqual(sut.currentPage, 2)
            }
        }
    }
    
    // MARK: - InnerTypes
    
    typealias EquatableData = Int
    
    struct NotEquatableData: ExpressibleByIntegerLiteral {
        typealias IntegerLiteralType = Int
                
        let value: Int
                
        init(integerLiteral value: Int) {
            self.value = value
        }
    }
    
}

fileprivate extension ViewModelDataState where TSuccess == Array<DataPaginationControllerTests.NotEquatableData>, TError == Never {
    var equatable: ViewModelDataState<[Int], Int> {
        switch self {
        case .failure(_): return .failure(error: 0)
        case .notLoaded: return .notLoaded
        case .loading(let data): return .loading(data: data?.map({ $0.value }))
        case .loaded(let data): return .loaded(data: data.map({ $0.value}))
        case .partialFailure(_, let data): return .partialFailure(error: 0, data: data.map({ $0.value}))
        }
    }
}
